import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { Action } from '@ngrx/store/src/models';

import { environment } from '../../environments/environment';
import { AuthState, authReducer } from './auth/auth.reducer';
import { AuthActionTypes } from './auth/auth.actions.types';
import { UserState, userReducer } from './user/user.reducer';

import * as fromProject from './project/project.reducer';
import * as fromTasks from './task/task.reducer';

export interface AppState {
  auth: AuthState;
  user: UserState;
  projects: fromProject.ProjectState;
  tasks: fromTasks.TaskState;
}

export const reducers: ActionReducerMap<AppState> = {
  auth: authReducer,
  user: userReducer,
  projects: fromProject.projectReducer,
  tasks: fromTasks.tasktReducer
};

export const metaReducers: MetaReducer<any>[] = !environment.production ? [clearState] : [clearState];

export function clearState(reducer: (arg0: any, arg1: any) => any) {
  return (state: any, action: Action) => {
    if (action.type === AuthActionTypes.LOGOUT) state = undefined;
    return reducer(state, action);
  };
}
