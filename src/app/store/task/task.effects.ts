import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, mergeMap, of, tap } from 'rxjs';
import { ModalRegisterService } from 'src/app/services/components/modal-components.service';
import { TaskService } from 'src/app/services/rest/task.service';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { UPDATE_ONE_PROJECT_ACTION } from '../project/project.actions';
import {
  ADD_ONE_TASK_ACTION,
  ADD_ONE_TASK_ERROR_ACTION,
  ADD_ONE_TASK_SUCCESS_ACTION,
  FETCH_ALL_TASKS,
  FETCH_ALL_TASKS_ERROR,
  FETCH_ALL_TASKS_SUCCESS,
  REMOVE_ONE_TASK_ACTION,
  REMOVE_ONE_TASK_ERROR_ACTION,
  REMOVE_ONE_TASK_SUCCESS_ACTION,
  UPDATE_ONE_TASK_ACTION,
  UPDATE_ONE_TASK_ERROR_ACTION,
  UPDATE_ONE_TASK_SUCCESS_ACTION
} from './task.actions';

@Injectable()
export class TaskEffects {
  constructor(
    private actions$: Actions,
    private taskService: TaskService,
    private modalRegisterService: ModalRegisterService
  ) {}

  addOneTask$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ADD_ONE_TASK_ACTION),
      exhaustMap((action) =>
        this.taskService.addOneTask(action.task).pipe(
          tap(() => {
            const modal: ModalComponent = this.modalRegisterService.getComponent(action.modalId);
            this.modalRegisterService.deregisterComponent(action.modalId);
            modal.close();
          }),
          mergeMap((task) => [
            ADD_ONE_TASK_SUCCESS_ACTION({ task }),
            UPDATE_ONE_PROJECT_ACTION({ modalId: '', project: { id: task.project?.id } })
          ]),
          catchError((error) => of(ADD_ONE_TASK_ERROR_ACTION({ error })))
        )
      )
    );
  });

  updateOneTask$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UPDATE_ONE_TASK_ACTION),
      exhaustMap((action) =>
        this.taskService.updateOneTask(action.task).pipe(
          mergeMap((task) => [
            UPDATE_ONE_TASK_SUCCESS_ACTION({ id: task.id!, changes: task }),
            UPDATE_ONE_PROJECT_ACTION({ modalId: '', project: { id: action.task.project?.id } })
          ]),
          catchError((error) => of(UPDATE_ONE_TASK_ERROR_ACTION({ error })))
        )
      )
    );
  });

  removeOneTask$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(REMOVE_ONE_TASK_ACTION),
      exhaustMap((action) =>
        this.taskService.removeOneTask(action.task).pipe(
          mergeMap(() => [
            REMOVE_ONE_TASK_SUCCESS_ACTION({ id: action.task.id! }),
            UPDATE_ONE_PROJECT_ACTION({ modalId: '', project: { id: action.task.project?.id } })
          ]),
          catchError((error) => of(REMOVE_ONE_TASK_ERROR_ACTION({ error })))
        )
      )
    );
  });

  getAllTasksByUserId$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_ALL_TASKS),
      exhaustMap((action) =>
        this.taskService.getAllTasksByUserId(action.projectId).pipe(
          map((tasks) => FETCH_ALL_TASKS_SUCCESS({ tasks })),
          catchError((error) => of(FETCH_ALL_TASKS_ERROR({ error })))
        )
      )
    );
  });
}
