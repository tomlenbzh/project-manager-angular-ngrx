import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, map, Observable } from 'rxjs';
import { IAddTask, ITask } from 'src/app/modules/projects/utils/interfaces/task.interface';
import { AppState } from '..';
import { ADD_ONE_TASK_ACTION, FETCH_ALL_TASKS, REMOVE_ONE_TASK_ACTION, UPDATE_ONE_TASK_ACTION } from './task.actions';
import { selectAllTasks } from './task.selectors';

@Injectable({ providedIn: 'root' })
export class TaskHelper {
  constructor(private store: Store<AppState>) {}

  tasks(): Observable<ITask[] | null> {
    return this.store.select(selectAllTasks);
  }

  onGoingTasks(): Observable<ITask[] | null> {
    return this.tasks().pipe(map((tasks) => tasks?.filter((task) => !task.completed) || []));
  }

  completedTasks(): Observable<ITask[] | null> {
    return this.tasks().pipe(map((tasks) => tasks?.filter((task) => task.completed) || []));
  }

  addOneTask(payload: IAddTask): void {
    const { modalId, task } = payload;
    this.store.dispatch(ADD_ONE_TASK_ACTION({ modalId, task }));
  }

  updateOneTask(task: ITask): void {
    this.store.dispatch(UPDATE_ONE_TASK_ACTION({ task }));
  }

  removeOneTask(task: ITask): void {
    this.store.dispatch(REMOVE_ONE_TASK_ACTION({ task }));
  }

  getAllTasksByProjectId(projectId: number): void {
    this.store.dispatch(FETCH_ALL_TASKS({ projectId }));
  }
}
