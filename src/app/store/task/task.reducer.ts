import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { ITask } from 'src/app/modules/projects/utils/interfaces/task.interface';
import {
  ADD_ONE_TASK_SUCCESS_ACTION,
  FETCH_ALL_TASKS_SUCCESS,
  REMOVE_ONE_TASK_SUCCESS_ACTION,
  UPDATE_ONE_TASK_SUCCESS_ACTION
} from './task.actions';

export interface TaskState extends EntityState<ITask> {
  selectedTaskId: string | null;
}

export const adapter = createEntityAdapter<ITask>({
  selectId: (task: ITask) => task.id!
});

export const initialState: TaskState = adapter.getInitialState({ selectedTaskId: null });

export const tasktReducer = createReducer(
  initialState,
  on(FETCH_ALL_TASKS_SUCCESS, (state: TaskState, { tasks }) => adapter.setAll(tasks, state)),
  on(ADD_ONE_TASK_SUCCESS_ACTION, (state: TaskState, { task }) => adapter.addOne(task, state)),
  on(UPDATE_ONE_TASK_SUCCESS_ACTION, (state: TaskState, { id, changes }) => adapter.updateOne({ id, changes }, state)),
  on(REMOVE_ONE_TASK_SUCCESS_ACTION, (state: TaskState, { id }) => adapter.removeOne(id, state))
);

export const getSelectedTaskId = (state: TaskState) => state.selectedTaskId;

const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

export const selectTaskIds = selectIds;
export const selectTaskEntities = selectEntities;
export const selectAllTasks = selectAll;
export const selectTaskTotal = selectTotal;
