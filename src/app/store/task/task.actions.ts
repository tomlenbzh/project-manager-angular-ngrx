import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { ITask } from 'src/app/modules/projects/utils/interfaces/task.interface';
import { TaskActionTypes } from './task.actions.types';

/**
 * FETCH ACTIONS
 */
export const FETCH_ALL_TASKS = createAction(TaskActionTypes.FETCH_ALL_TASKS, props<{ projectId: number }>());
export const FETCH_ALL_TASKS_SUCCESS = createAction(
  TaskActionTypes.FETCH_ALL_TASKS_SUCCESS,
  props<{ tasks: ITask[] }>()
);
export const FETCH_ALL_TASKS_ERROR = createAction(
  TaskActionTypes.FETCH_ALL_TASKS_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * ADD ACTIONS
 */
export const ADD_ONE_TASK_ACTION = createAction(
  TaskActionTypes.ADD_ONE_TASK,
  props<{ modalId: string; task: ITask }>()
);
export const ADD_ONE_TASK_SUCCESS_ACTION = createAction(TaskActionTypes.ADD_ONE_TASK_SUCCESS, props<{ task: ITask }>());
export const ADD_ONE_TASK_ERROR_ACTION = createAction(
  TaskActionTypes.ADD_ONE_TASK_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * UPDATE ACTIONS
 */
export const UPDATE_ONE_TASK_ACTION = createAction(TaskActionTypes.UPDATE_ONE_TASK, props<{ task: ITask }>());
export const UPDATE_ONE_TASK_SUCCESS_ACTION = createAction(
  TaskActionTypes.UPDATE_ONE_TASK_SUCCESS,
  props<{ id: number; changes: ITask }>()
);
export const UPDATE_ONE_TASK_ERROR_ACTION = createAction(
  TaskActionTypes.UPDATE_ONE_TASK_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * DELETE ACTIONS
 */
export const REMOVE_ONE_TASK_ACTION = createAction(TaskActionTypes.REMOVE_ONE_TASK, props<{ task: ITask }>());
export const REMOVE_ONE_TASK_SUCCESS_ACTION = createAction(
  TaskActionTypes.REMOVE_ONE_TASK_SUCCESS,
  props<{ id: number }>()
);
export const REMOVE_ONE_TASK_ERROR_ACTION = createAction(
  TaskActionTypes.REMOVE_ONE_TASK_ERROR,
  props<{ error: HttpErrorResponse }>()
);
