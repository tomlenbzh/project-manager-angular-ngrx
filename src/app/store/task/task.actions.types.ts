export enum TaskActionTypes {
  FETCH_ALL_TASKS = '[TASK] Fetch all',
  FETCH_ALL_TASKS_SUCCESS = '[TASK] Fetch all Success',
  FETCH_ALL_TASKS_ERROR = '[TASK] Fetch Error',
  ADD_ONE_TASK = '[TASK] Add one',
  ADD_ONE_TASK_SUCCESS = '[TASK] Add one Success',
  ADD_ONE_TASK_ERROR = '[TASK] Add one Error',
  UPDATE_ONE_TASK = '[TASK] Update one',
  UPDATE_ONE_TASK_SUCCESS = '[TASK] Update one Success',
  UPDATE_ONE_TASK_ERROR = '[TASK] Update one Error',
  REMOVE_ONE_TASK = '[TASK] Delete one',
  REMOVE_ONE_TASK_SUCCESS = '[TASK] Delete one Success',
  REMOVE_ONE_TASK_ERROR = '[TASK] Delete one Error'
}
