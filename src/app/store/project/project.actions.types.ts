export enum ProjectActionTypes {
  FETCH_ALL_PROJECTS = '[PROJECT] Fetch all',
  FETCH_ALL_PROJECTS_SUCCESS = '[PROJECT] Fetch all Success',
  FETCH_ALL_PROJECTS_ERROR = '[PROJECT] Fetch Error',
  ADD_ONE_PROJECT = '[PROJECT] Add one',
  ADD_ONE_PROJECT_SUCCESS = '[PROJECT] Add one Success',
  ADD_ONE_PROJECT_ERROR = '[PROJECT] Add one Error',
  UPDATE_ONE_PROJECT = '[PROJECT] Update one',
  UPDATE_ONE_PROJECT_SUCCESS = '[PROJECT] Update one Success',
  UPDATE_ONE_PROJECT_ERROR = '[PROJECT] Update one Error',
  REMOVE_ONE_PROJECT = '[PROJECT] Delete one',
  REMOVE_ONE_PROJECT_SUCCESS = '[PROJECT] Delete one Success',
  REMOVE_ONE_PROJECT_ERROR = '[PROJECT] Delete one Error'
}
