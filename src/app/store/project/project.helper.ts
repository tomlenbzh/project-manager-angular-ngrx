import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { IAddUpdateProject, IProject } from 'src/app/modules/projects/utils/interfaces/project.interface';
import { AppState } from '../';
import {
  ADD_ONE_PROJECT_ACTION,
  FETCH_ALL_PROJECTS_ACTION,
  REMOVE_ONE_PROJECT_ACTION,
  UPDATE_ONE_PROJECT_ACTION
} from './project.actions';
import { selectAllProjects } from './project.selectors';

// UPDATE_ONE_PROJECT_ACTION

@Injectable({ providedIn: 'root' })
export class ProjectHelper {
  constructor(private store: Store<AppState>) {}

  projects(): Observable<IProject[] | null> {
    return this.store.select(selectAllProjects);
  }

  onGoingProjects(): Observable<IProject[] | null> {
    return this.projects().pipe(
      map(
        (projects: IProject[] | null) =>
          projects
            ?.filter((project) => !project.completed)
            .sort((a: IProject, b: IProject) => new Date(b.updatedAt!).getTime() - new Date(a.updatedAt!).getTime()) ||
          []
      )
    );
  }

  completedProjects(): Observable<IProject[] | null> {
    return this.projects().pipe(
      map(
        (projects: IProject[] | null) =>
          projects
            ?.filter((project) => project.completed)
            .sort((a: IProject, b: IProject) => new Date(b.updatedAt!).getTime() - new Date(a.updatedAt!).getTime()) ||
          []
      )
    );
  }

  addOneProject(payload: IAddUpdateProject): void {
    const { modalId, project } = payload;
    this.store.dispatch(ADD_ONE_PROJECT_ACTION({ modalId, project }));
  }

  updateOneProject(payload: IAddUpdateProject): void {
    const { modalId, project } = payload;
    this.store.dispatch(UPDATE_ONE_PROJECT_ACTION({ modalId, project }));
  }

  removeOneProject(project: IProject): void {
    this.store.dispatch(REMOVE_ONE_PROJECT_ACTION({ project }));
  }

  getAllProjectsByUserId(userId: number): void {
    this.store.dispatch(FETCH_ALL_PROJECTS_ACTION({ userId }));
  }
}
