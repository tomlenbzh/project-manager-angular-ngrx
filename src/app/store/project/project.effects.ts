import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, of, tap } from 'rxjs';
import { ModalRegisterService } from 'src/app/services/components/modal-components.service';
import { ProjectService } from 'src/app/services/rest/project.service';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import {
  ADD_ONE_PROJECT_ACTION,
  ADD_ONE_PROJECT_SUCCESS_ACTION,
  ADD_ONE_PROJECT_ERROR_ACTION,
  FETCH_ALL_PROJECTS_ACTION,
  FETCH_ALL_PROJECTS_ERROR_ACTION,
  FETCH_ALL_PROJECTS_SUCCESS_ACTION,
  UPDATE_ONE_PROJECT_ACTION,
  UPDATE_ONE_PROJECT_SUCCESS_ACTION,
  UPDATE_ONE_PROJECT_ERROR_ACTION,
  REMOVE_ONE_PROJECT_ACTION,
  REMOVE_ONE_PROJECT_SUCCESS_ACTION,
  REMOVE_ONE_PROJECT_ERROR_ACTION
} from './project.actions';

@Injectable()
export class ProjectEffects {
  constructor(
    private actions$: Actions,
    private projectService: ProjectService,
    private modalRegisterService: ModalRegisterService
  ) {}

  addOneProject$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ADD_ONE_PROJECT_ACTION),
      exhaustMap((action) =>
        this.projectService.addOneProject(action.project).pipe(
          tap(() => {
            const modal: ModalComponent = this.modalRegisterService.getComponent(action.modalId);
            modal && this.modalRegisterService.deregisterComponent(action.modalId);
            modal && modal.close();
          }),
          map((project) => ADD_ONE_PROJECT_SUCCESS_ACTION({ project })),
          catchError((error) => of(ADD_ONE_PROJECT_ERROR_ACTION({ error })))
        )
      )
    );
  });

  updateOneProject$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UPDATE_ONE_PROJECT_ACTION),
      exhaustMap((action) =>
        this.projectService.updateOneProject(action.project).pipe(
          tap(() => {
            const modal: ModalComponent = this.modalRegisterService.getComponent(action.modalId);
            modal && this.modalRegisterService.deregisterComponent(action.modalId);
            modal && modal.close();
          }),
          map((project) => UPDATE_ONE_PROJECT_SUCCESS_ACTION({ id: project.id!, changes: project })),
          catchError((error) => of(UPDATE_ONE_PROJECT_ERROR_ACTION({ error })))
        )
      )
    );
  });

  removeOneProject$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(REMOVE_ONE_PROJECT_ACTION),
      exhaustMap((action) =>
        this.projectService.removeOneProject(action.project).pipe(
          map(() => REMOVE_ONE_PROJECT_SUCCESS_ACTION({ id: action.project.id! })),
          catchError((error) => of(REMOVE_ONE_PROJECT_ERROR_ACTION({ error })))
        )
      )
    );
  });

  getAllProjectsByUserId$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_ALL_PROJECTS_ACTION),
      exhaustMap((action) =>
        this.projectService.getAllProjectsByUserId(action.userId).pipe(
          map((projects) => FETCH_ALL_PROJECTS_SUCCESS_ACTION({ projects })),
          catchError((error) => of(FETCH_ALL_PROJECTS_ERROR_ACTION({ error })))
        )
      )
    );
  });
}
