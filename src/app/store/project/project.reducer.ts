import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { IProject } from 'src/app/modules/projects/utils/interfaces/project.interface';
import {
  ADD_ONE_PROJECT_SUCCESS_ACTION,
  REMOVE_ONE_PROJECT_SUCCESS_ACTION,
  FETCH_ALL_PROJECTS_SUCCESS_ACTION,
  UPDATE_ONE_PROJECT_SUCCESS_ACTION
} from './project.actions';

export interface ProjectState extends EntityState<IProject> {
  selectedProjectId: string | null;
}

export const adapter = createEntityAdapter<IProject>({
  selectId: (project: IProject) => project.id!
});

export const initialState: ProjectState = adapter.getInitialState({ selectedProjectId: null });

export const projectReducer = createReducer(
  initialState,
  on(FETCH_ALL_PROJECTS_SUCCESS_ACTION, (state: ProjectState, { projects }) => adapter.setAll(projects, state)),
  on(ADD_ONE_PROJECT_SUCCESS_ACTION, (state: ProjectState, { project }) => adapter.addOne(project, state)),
  on(UPDATE_ONE_PROJECT_SUCCESS_ACTION, (state: ProjectState, { id, changes }) =>
    adapter.updateOne({ id, changes }, state)
  ),
  on(REMOVE_ONE_PROJECT_SUCCESS_ACTION, (state: ProjectState, { id }) => adapter.removeOne(id, state))
);

export const getSelectedProjectId = (state: ProjectState) => state.selectedProjectId;

const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

export const selectProjectIds = selectIds;
export const selectProjectEntities = selectEntities;
export const selectAllProjects = selectAll;
export const selectProjectTotal = selectTotal;
