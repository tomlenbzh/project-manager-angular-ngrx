import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromProject from './project.reducer';

export const selectProjectState = createFeatureSelector<fromProject.ProjectState>('projects');

export const selectProjectIds = createSelector(selectProjectState, fromProject.selectProjectIds);

export const selectProjectEntities = createSelector(selectProjectState, fromProject.selectProjectEntities);

export const selectAllProjects = createSelector(selectProjectState, fromProject.selectAllProjects);

export const selectProjectTotal = createSelector(selectProjectState, fromProject.selectProjectTotal);

export const selectCurrentProjectId = createSelector(selectProjectState, fromProject.getSelectedProjectId);

export const selectCurrentProject = createSelector(
  selectProjectEntities,
  selectCurrentProjectId,
  (ProjectEntities, ProjectId) => ProjectId && ProjectEntities[ProjectId]
);
