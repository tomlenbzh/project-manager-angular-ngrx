import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IProject } from 'src/app/modules/projects/utils/interfaces/project.interface';
import { ProjectActionTypes } from './project.actions.types';

/**
 * FETCH ACTIONS
 */
export const FETCH_ALL_PROJECTS_ACTION = createAction(
  ProjectActionTypes.FETCH_ALL_PROJECTS,
  props<{ userId: number }>()
);
export const FETCH_ALL_PROJECTS_SUCCESS_ACTION = createAction(
  ProjectActionTypes.FETCH_ALL_PROJECTS_SUCCESS,
  props<{ projects: IProject[] }>()
);
export const FETCH_ALL_PROJECTS_ERROR_ACTION = createAction(
  ProjectActionTypes.FETCH_ALL_PROJECTS_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * ADD ACTIONS
 */
export const ADD_ONE_PROJECT_ACTION = createAction(
  ProjectActionTypes.ADD_ONE_PROJECT,
  props<{ modalId: string; project: IProject }>()
);
export const ADD_ONE_PROJECT_SUCCESS_ACTION = createAction(
  ProjectActionTypes.ADD_ONE_PROJECT_SUCCESS,
  props<{ project: IProject }>()
);
export const ADD_ONE_PROJECT_ERROR_ACTION = createAction(
  ProjectActionTypes.ADD_ONE_PROJECT_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * UPDATE ACTIONS
 */
export const UPDATE_ONE_PROJECT_ACTION = createAction(
  ProjectActionTypes.UPDATE_ONE_PROJECT,
  props<{ modalId: string; project: IProject }>()
);
export const UPDATE_ONE_PROJECT_SUCCESS_ACTION = createAction(
  ProjectActionTypes.UPDATE_ONE_PROJECT_SUCCESS,
  props<{ id: number; changes: IProject }>()
);
export const UPDATE_ONE_PROJECT_ERROR_ACTION = createAction(
  ProjectActionTypes.UPDATE_ONE_PROJECT_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * DELETE ACTIONS
 */
export const REMOVE_ONE_PROJECT_ACTION = createAction(
  ProjectActionTypes.REMOVE_ONE_PROJECT,
  props<{ project: IProject }>()
);
export const REMOVE_ONE_PROJECT_SUCCESS_ACTION = createAction(
  ProjectActionTypes.REMOVE_ONE_PROJECT_SUCCESS,
  props<{ id: number }>()
);
export const REMOVE_ONE_PROJECT_ERROR_ACTION = createAction(
  ProjectActionTypes.REMOVE_ONE_PROJECT_ERROR,
  props<{ error: HttpErrorResponse }>()
);
