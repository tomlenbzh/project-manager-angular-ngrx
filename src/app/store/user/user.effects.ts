import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import {
  FETCH_USER_ACTION,
  FETCH_USER_ACTION_SUCCESS,
  FETCH_USER_ACTION_ERROR,
  UPDATE_USER_ACTION,
  UPDATE_USER_ACTION_SUCCESS,
  UPDATE_USER_ACTION_ERROR
} from './user.actions';
import { IUser } from 'src/app/modules/auth/utils/user.interface';
import { UserService } from 'src/app/services/rest/user.service';
import { TranslateService } from '@ngx-translate/core';
import { SELECTED_LANGUAGE } from 'src/app/utils/constants/lang';

@Injectable()
export class UserEffects {
  constructor(private actions$: Actions, private userService: UserService, private translate: TranslateService) {}

  fetchUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_USER_ACTION),
      exhaustMap((action) =>
        this.userService.fetchUser(action.id).pipe(
          map((user: IUser) => FETCH_USER_ACTION_SUCCESS({ user })),
          catchError((error) => of(FETCH_USER_ACTION_ERROR({ error })))
        )
      )
    );
  });

  updateUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UPDATE_USER_ACTION),
      exhaustMap((action) =>
        this.userService.updateUser(action.user).pipe(
          map((user: IUser) => UPDATE_USER_ACTION_SUCCESS({ user })),
          catchError((error) => of(UPDATE_USER_ACTION_ERROR({ error })))
        )
      )
    );
  });
}
