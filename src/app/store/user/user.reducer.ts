import { createReducer, on } from '@ngrx/store';
import { IUser } from 'src/app/modules/auth/utils/user.interface';
import {
  FETCH_USER_ACTION,
  FETCH_USER_ACTION_ERROR,
  FETCH_USER_ACTION_SUCCESS,
  UPDATE_USER_ACTION,
  UPDATE_USER_ACTION_ERROR,
  UPDATE_USER_ACTION_SUCCESS
} from './user.actions';

export interface UserState {
  isLoading: boolean;
  user: IUser | null;
}

export const initialState: UserState = {
  isLoading: false,
  user: null
};

export const userReducer = createReducer(
  initialState,
  on(FETCH_USER_ACTION, (state: UserState): UserState => ({ ...state, isLoading: true })),
  on(FETCH_USER_ACTION_SUCCESS, (state: UserState, { user }): UserState => ({ ...state, isLoading: false, user })),
  on(FETCH_USER_ACTION_ERROR, (state: UserState, { error }): UserState => ({ ...state, isLoading: false })),
  on(UPDATE_USER_ACTION, (state: UserState): UserState => ({ ...state, isLoading: true })),
  on(UPDATE_USER_ACTION_SUCCESS, (state: UserState, { user }): UserState => ({ ...state, isLoading: false, user })),
  on(UPDATE_USER_ACTION_ERROR, (state: UserState, { error }): UserState => ({ ...state, isLoading: false }))
);
