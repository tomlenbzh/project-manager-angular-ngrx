import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IUser } from 'src/app/modules/auth/utils/user.interface';
import { UserActionTypes } from './user.actions.types';

export const FETCH_USER_ACTION = createAction(UserActionTypes.FETCH_USER, props<{ id: number }>());
export const FETCH_USER_ACTION_SUCCESS = createAction(UserActionTypes.FETCH_USER_SUCCESS, props<{ user: IUser }>());
export const FETCH_USER_ACTION_ERROR = createAction(
  UserActionTypes.FETCH_USER_ERROR,
  props<{ error: HttpErrorResponse }>()
);

export const UPDATE_USER_ACTION = createAction(UserActionTypes.UPDATE_USER, props<{ user: IUser }>());
export const UPDATE_USER_ACTION_SUCCESS = createAction(UserActionTypes.UPDATE_USER_SUCCESS, props<{ user: IUser }>());
export const UPDATE_USER_ACTION_ERROR = createAction(
  UserActionTypes.UPDATE_USER_ERROR,
  props<{ error: HttpErrorResponse }>()
);
