import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/modules/auth/utils/user.interface';
import { AppState } from 'src/app/store';
import { FETCH_USER_ACTION, UPDATE_USER_ACTION } from './user.actions';
import { selectUser, selectUserLoading } from './user.selectors';

@Injectable({
  providedIn: 'root'
})
export class UserHelper {
  constructor(private store: Store<AppState>) {}

  isLoading(): Observable<boolean> {
    return this.store.select(selectUserLoading);
  }

  user(): Observable<IUser | null> {
    return this.store.select(selectUser);
  }

  fetchUser(id: number): void {
    this.store.dispatch(FETCH_USER_ACTION({ id }));
  }

  updateUser(user: IUser): void {
    this.store.dispatch(UPDATE_USER_ACTION({ user }));
  }
}
