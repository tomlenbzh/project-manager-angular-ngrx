import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IUser } from '../../modules/auth/utils/user.interface';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(private httpClient: HttpClient) {}

  login(credentials: IUser): Observable<any> {
    const url = `${environment.baseUrl}/users/login`;
    return this.httpClient.post<any>(url, credentials);
  }

  signUp(credentials: IUser): Observable<any> {
    const url = `${environment.baseUrl}/users`;
    return this.httpClient.post<any>(url, credentials);
  }
}
