import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ITask } from 'src/app/modules/projects/utils/interfaces/task.interface';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class TaskService {
  constructor(private httpClient: HttpClient) {}

  getAllTasksByUserId(userId: number): Observable<ITask[]> {
    return this.httpClient.get<ITask[]>(`${environment.baseUrl}/tasks/project/${userId}`);
  }

  addOneTask(task: ITask): Observable<ITask> {
    return this.httpClient.post<ITask>(`${environment.baseUrl}/tasks`, task);
  }

  updateOneTask(task: ITask): Observable<ITask> {
    return this.httpClient.put<ITask>(`${environment.baseUrl}/tasks/${task.id}`, task);
  }

  removeOneTask(task: ITask): Observable<ITask> {
    return this.httpClient.delete<ITask>(`${environment.baseUrl}/tasks/${task.id}`);
  }
}
