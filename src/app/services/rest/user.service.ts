import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/modules/auth/utils/user.interface';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private httpClient: HttpClient) {}

  fetchUser(id: number): Observable<IUser> {
    return this.httpClient.get<IUser>(`${environment.baseUrl}/users/${id}`);
  }

  updateUser(user: IUser): Observable<IUser> {
    return this.httpClient.put<IUser>(`${environment.baseUrl}/users/${user.id}`, user);
  }
}
