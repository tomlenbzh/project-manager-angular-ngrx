import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IProject } from 'src/app/modules/projects/utils/interfaces/project.interface';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ProjectService {
  constructor(private httpClient: HttpClient) {}

  getAllProjectsByUserId(userId: number): Observable<IProject[]> {
    return this.httpClient.get<IProject[]>(`${environment.baseUrl}/projects/user/${userId}`);
  }

  addOneProject(project: IProject): Observable<IProject> {
    return this.httpClient.post<IProject>(`${environment.baseUrl}/projects`, project);
  }

  updateOneProject(project: IProject): Observable<IProject> {
    return this.httpClient.put<IProject>(`${environment.baseUrl}/projects/${project.id}`, project);
  }

  removeOneProject(project: IProject): Observable<IProject> {
    return this.httpClient.delete<IProject>(`${environment.baseUrl}/projects/${project.id}`);
  }
}
