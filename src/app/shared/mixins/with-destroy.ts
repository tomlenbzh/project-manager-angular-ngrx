import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

/**
 * WithDestroy mixin handles unsubscription with destroyed$ property (rxjs rubject)
 */
export function WithDestroy(Base = class {} as any) {
  @Component({ template: '' })
  class TemporaryComponent extends Base implements OnDestroy {
    protected destroyed$: Subject<boolean> = new Subject<boolean>();

    ngOnDestroy(): void {
      if (typeof super['ngOnDestroy'] === 'function') super.ngOnDestroy();
      this.unsubscribe();
    }

    protected unsubscribe(): void {
      this.destroyed$.next(true);
      this.destroyed$.unsubscribe();
    }
  }

  return TemporaryComponent;
}
