import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModules } from './modules/material.module';
import { components } from './components';
import { ModalComponent } from './components/modal/modal.component';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [...components, ModalComponent],
  imports: [CommonModule, MaterialModules, TranslateModule.forChild(), FormsModule, ReactiveFormsModule],
  exports: [MaterialModules, ...components, MaterialModules]
})
export class SharedModule {}
