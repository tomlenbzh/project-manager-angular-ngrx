import { NoopScrollStrategy, Overlay } from '@angular/cdk/overlay';
import { DOCUMENT } from '@angular/common';
import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { takeUntil } from 'rxjs';
import { ModalRegisterService } from 'src/app/services/components/modal-components.service';
import { generateRandomId } from 'src/app/utils/methods/generate-random-id';
import { WithDestroy } from '../../mixins/with-destroy';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent extends WithDestroy() implements OnInit, OnChanges, OnDestroy {
  @Input() id = generateRandomId(10);
  @Input() title!: string;
  @Input() form = new FormGroup({});
  @Input() hasActions = true;

  @Output() opened: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() closed: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() submitted: EventEmitter<any> = new EventEmitter<any>();
  @Output() infiniteScroll: EventEmitter<any> = new EventEmitter();

  @ViewChild('modalTemplate', { static: true }) private modalTemplate!: TemplateRef<any>;

  dialogRef: MatDialogRef<any, any> | undefined;

  private config: MatDialogConfig = {
    autoFocus: false,
    panelClass: 'sm',
    restoreFocus: false,
    scrollStrategy: new NoopScrollStrategy()
  };

  constructor(
    private modalRegisterService: ModalRegisterService,
    private dialog: MatDialog,
    @Inject(DOCUMENT) private document: Document,
    private overlay: Overlay
  ) {
    super();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.modalRegisterService.registerComponent(this.id, this);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('form' in changes) {
      if (this.form === null) this.form = new FormGroup({});
    }
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Creates a new MatDialog instance. Subscribes to open / close events.
   *
   * @param     { any }                 template
   * @param     { MatDialogConfig }     config
   */
  open(config?: MatDialogConfig): void {
    this.dialogRef = this.dialog.open(this.modalTemplate, {
      ...this.config,
      ...config,
      scrollStrategy: this.overlay.scrollStrategies.block()
    });
    this.modalRegisterService.registerComponent(this.id, this.dialogRef);

    this.dialogRef
      .afterOpened()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.opened.emit(true));

    this.dialogRef
      .afterClosed()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => {
        this.closed.emit(true);
        this.modalRegisterService.deregisterComponent(this.id);
        this.dialogRef = undefined;
      });
  }

  /**
   * Closes the current dialog instance thanks to its reference.
   *
   * @param     { string }      value
   */
  close(value?: any): void {
    this.dialogRef?.close(value);
  }

  /**
   * Emits the form value if form exists, is valid and has been submitted.
   */
  onFormSubmitted(): void {
    if (this.form && this.form.valid) {
      this.submitted.emit(this.form.getRawValue());
    }
  }
}
