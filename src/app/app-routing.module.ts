import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutContainerComponent } from './core/containers/main-layout/main-layout-container.component';
import { AuthenticationGuard } from './shared/guards/auth.guard';

const layoutChildren: Routes = [
  {
    path: 'projects',
    loadChildren: () => import('./modules/projects/projects.module').then((m) => m.ProjectsModule)
  },
  {
    path: '',
    redirectTo: 'projects',
    pathMatch: 'full'
  }
];

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then((m) => m.AuthModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'app',
    component: MainLayoutContainerComponent,
    children: layoutChildren,
    canActivate: [AuthenticationGuard]
  },
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: '**', redirectTo: 'app' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
