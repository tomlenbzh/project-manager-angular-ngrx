export enum LANG {
  FR = 'fr',
  EN = 'en'
}

export const SELECTED_LANGUAGE = 'selectedLang';
export const ACCEPTED_LANGUAGES = [LANG.FR, LANG.EN];
