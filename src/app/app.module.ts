import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

@NgModule({
  imports: [CoreModule.forRoot(), AppRoutingModule, BrowserAnimationsModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
