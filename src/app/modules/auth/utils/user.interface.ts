export enum UserRole {
  ADMIN = 'admin',
  USER = 'user'
}

export interface IUser {
  id?: number;
  userName?: string;
  email?: string;
  password?: string;
  lang?: string;
}

export interface IPartialUser {
  id: number;
}

export interface LoginInfo {
  token: string;
  user: IUser;
}
