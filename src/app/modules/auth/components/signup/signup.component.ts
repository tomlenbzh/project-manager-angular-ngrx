import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { TranslateService } from '@ngx-translate/core';
import { ACCEPTED_LANGUAGES, LANG, SELECTED_LANGUAGE } from 'src/app/utils/constants/lang';
import { SignupFormControlsNames } from '../../utils/form.constants';
import { IUser } from '../../utils/user.interface';
import { passwordMatchValidator } from '../../utils/validators/check-password';
import { CustomErrorStateMatcher } from '../../utils/validators/state-matcher';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  @Input() isLoading!: boolean | null;
  @Input() errorMessage!: string | null;

  @Output() signUpClicked: EventEmitter<IUser> = new EventEmitter<IUser>();

  controlNames = SignupFormControlsNames;
  form!: FormGroup;
  stateMatcher = new CustomErrorStateMatcher();

  languages = ACCEPTED_LANGUAGES;
  selectedLang!: string;

  constructor(private formBuilder: FormBuilder, private translate: TranslateService) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  get emailCtrl() {
    return this.form.get(this.controlNames.EMAIL);
  }

  get passwordCtrl() {
    return this.form.get(this.controlNames.PASSWORD);
  }

  get passwordConfirmCtrl() {
    return this.form.get(this.controlNames.PASSWORD_CONFIRMATION);
  }

  get userNameCtrl() {
    return this.form.get(this.controlNames.USERNAME);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    const currentLang = localStorage.getItem(SELECTED_LANGUAGE);
    if (currentLang) {
      this.selectedLang = currentLang;
    } else {
      const browserLang = navigator.language;
      this.selectedLang = this.languages.includes(browserLang as LANG) ? browserLang : LANG.EN;
    }

    this.createForm();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  onChangeLang(change: MatRadioChange): void {
    const lang = change.value;
    this.translate.use(lang);
  }

  /**
   * Emits the user's information to the parent component.
   */
  submitForm(): void {
    if (this.form.valid) {
      const credentials: IUser = this.form.getRawValue();
      credentials.lang = this.translate.currentLang;
      this.signUpClicked.emit(credentials);
    }
  }

  /**
   * Sets or removes errors when password is edited.
   */
  onPasswordInput(): void {
    this.passwordConfirmCtrl?.setErrors(this.form.hasError('passwordMismatch') ? [{ passwordMismatch: true }] : null);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Creates a new formGroup instance.
   */
  private createForm(): void {
    this.form = this.formBuilder.group(
      {
        [this.controlNames.EMAIL]: new FormControl('', [Validators.required, Validators.email]),
        [this.controlNames.USERNAME]: new FormControl('', [Validators.required]),
        [this.controlNames.PASSWORD]: new FormControl('', [Validators.required]),
        [this.controlNames.PASSWORD_CONFIRMATION]: new FormControl('', [Validators.required])
      },
      { validator: passwordMatchValidator }
    );
  }
}
