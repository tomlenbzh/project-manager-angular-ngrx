import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, takeUntil } from 'rxjs';
import { IUser } from 'src/app/modules/auth/utils/user.interface';
import { WithDestroy } from 'src/app/shared/mixins/with-destroy';
import { ProjectHelper } from 'src/app/store/project/project.helper';
import { UserHelper } from 'src/app/store/user/user.helper';
import { IProject, IAddUpdateProject } from '../../utils/interfaces/project.interface';

@Component({
  selector: 'app-projects-container',
  template: `<app-projects
    [onGoingProjects]="onGoingProjects | async"
    [completedProjects]="completedProjects | async"
    [userId]="userId"
    (created)="addOneProject($event)"
    (edited)="updateOneProject($event)"
    (deleted)="removeOneProject($event)"
  ></app-projects>`
})
export class ProjectsContainerComponent extends WithDestroy() implements OnInit, OnDestroy {
  onGoingProjects!: Observable<IProject[] | null>;
  completedProjects!: Observable<IProject[] | null>;

  userId!: number;

  constructor(private userHelper: UserHelper, private projectHelper: ProjectHelper) {
    super();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.userHelper
      .user()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((user: IUser | null) => {
        this.userId = user?.id!;

        if (this.userId) {
          this.projectHelper.getAllProjectsByUserId(this.userId);
          this.onGoingProjects = this.projectHelper.onGoingProjects();
          this.completedProjects = this.projectHelper.completedProjects();
        }
      });
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  addOneProject(payload: IAddUpdateProject): void {
    payload.project.user = { id: this.userId };
    this.projectHelper.addOneProject(payload);
  }

  updateOneProject(payload: IAddUpdateProject): void {
    this.projectHelper.updateOneProject(payload);
  }

  removeOneProject(project: IProject): void {
    this.projectHelper.removeOneProject(project);
  }
}
