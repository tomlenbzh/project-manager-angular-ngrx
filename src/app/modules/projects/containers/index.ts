import { ProjectTaskListContainerComponent } from './project-task-list/project-task-list-container.component';
import { ProjectsContainerComponent } from './projects/projects-container.component';

export const containers = [ProjectsContainerComponent, ProjectTaskListContainerComponent];
