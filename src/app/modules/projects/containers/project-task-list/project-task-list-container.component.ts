import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskHelper } from 'src/app/store/task/task.helper';
import { IProject } from '../../utils/interfaces/project.interface';
import { IAddTask, ITask } from '../../utils/interfaces/task.interface';

@Component({
  selector: 'app-project-task-list-container',
  template: `<app-project-task-list
    [project]="project"
    [completedTasks]="completedTasks | async"
    [onGoingTasks]="onGoingTasks | async"
    (created)="addOneTask($event)"
    (updated)="updateOneTask($event)"
    (deleted)="removeOneTask($event)"
  ></app-project-task-list>`
})
export class ProjectTaskListContainerComponent implements OnInit, OnChanges {
  @Input() project!: IProject | null;
  @Input() userId!: number;

  completedTasks!: Observable<ITask[] | null>;
  onGoingTasks!: Observable<ITask[] | null>;

  constructor(private taskHelper: TaskHelper) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecyclehooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.completedTasks = this.taskHelper.completedTasks();
    this.onGoingTasks = this.taskHelper.onGoingTasks();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('project' in changes && this.project) {
      this.taskHelper.getAllTasksByProjectId(this.project.id!);
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  addOneTask(payload: IAddTask): void {
    payload.task.project = { id: this.project?.id! };
    payload.task.user = { id: this.userId! };
    this.taskHelper.addOneTask(payload);
  }

  updateOneTask(task: ITask): void {
    const updatedTask: ITask = { ...task, project: { id: this.project?.id! } };
    this.taskHelper.updateOneTask(updatedTask);
  }

  removeOneTask(task: ITask): void {
    const removedTask: ITask = { ...task, project: { id: this.project?.id! } };
    this.taskHelper.removeOneTask(removedTask);
  }
}
