import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjectTaskListContainerComponent } from './project-task-list-container.component';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from '../../../../store';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { ProjectTaskListComponent } from '../../components/project-task-list/project-task-list.component';

describe('ProjectTaskListContainerComponent', () => {
  let component: ProjectTaskListContainerComponent;
  let fixture: ComponentFixture<ProjectTaskListContainerComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectTaskListContainerComponent, ProjectTaskListComponent],
      imports: [
        TranslateModule.forRoot(),
        StoreModule.forRoot(fromRoot.reducers),
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        SharedModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ProjectTaskListContainerComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
