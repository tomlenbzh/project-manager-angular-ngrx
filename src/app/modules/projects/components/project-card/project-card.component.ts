import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { IProject } from '../../utils/interfaces/project.interface';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent implements OnChanges {
  @Input() project!: IProject;

  @Output() edited: EventEmitter<IProject> = new EventEmitter<IProject>();
  @Output() completed: EventEmitter<IProject> = new EventEmitter<IProject>();
  @Output() deleted: EventEmitter<IProject> = new EventEmitter<IProject>();
  @Output() selected: EventEmitter<IProject> = new EventEmitter<IProject>();

  updatedAt: any;
  startsOn: any;
  dueOn: any;
  completion!: number;

  isOnGoing = false;
  isExpired = false;

  constructor(private translate: TranslateService) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('project' in changes && this.project) {
      const locale = this.translate.currentLang;

      this.updatedAt = moment(this.project.updatedAt).locale(locale)?.fromNow();
      this.startsOn = moment(this.project.startsOn).locale(locale)?.fromNow();
      this.dueOn = moment(this.project.dueOn).locale(locale)?.fromNow();

      this.completion = Number(((100 * this.project.tasksDone!) / this.project.tasksTotal! || 0).toFixed());

      this.isOnGoing = this.checkIsOnGoing();
      this.isExpired = this.checkIsOverdue();
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  setStatus(): void {
    const completed = !this.project.completed;
    const project: IProject = { ...this.project, completed };
    this.completed.emit(project);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private checkIsOnGoing(): boolean {
    if (!this.project.completed && this.project?.startsOn && this.project.dueOn) {
      const now = new Date();
      const startsOn = new Date(this.project.startsOn);
      const dueOn = new Date(this.project.dueOn);
      return startsOn < now && dueOn > now ? true : false;
    } else {
      return false;
    }
  }

  private checkIsOverdue(): boolean {
    if (!this.project?.completed && this.project.dueOn) {
      const now = new Date();
      const dueOn = new Date(this.project.dueOn);
      return dueOn < now ? true : false;
    } else {
      return false;
    }
  }
}
