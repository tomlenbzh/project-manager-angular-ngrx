import { ProjectCardComponent } from './project-card/project-card.component';
import { ProjectTaskListComponent } from './project-task-list/project-task-list.component';
import { ProjectTaskComponent } from './project-task/project-task.component';
import { ProjectsComponent } from './projects/projects.components';

export const components = [ProjectsComponent, ProjectCardComponent, ProjectTaskListComponent, ProjectTaskComponent];
