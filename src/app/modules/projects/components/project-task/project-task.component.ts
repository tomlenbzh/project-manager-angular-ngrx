import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ITask } from '../../utils/interfaces/task.interface';

@Component({
  selector: 'app-project-task',
  templateUrl: './project-task.component.html',
  styleUrls: ['./project-task.component.scss']
})
export class ProjectTaskComponent implements OnChanges {
  @Input() task!: ITask;

  @Output() private updated: EventEmitter<ITask> = new EventEmitter<ITask>();
  @Output() private deleted: EventEmitter<ITask> = new EventEmitter<ITask>();

  completed = false;

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('task' in changes && this.task) {
      this.completed = this.task.completed!;
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  onCheck($event: MatCheckboxChange): void {
    const completed = $event.checked;
    const task: ITask = { ...this.task, completed };
    this.updated.emit(task);
  }

  onDelete(): void {
    this.deleted.emit(this.task);
  }
}
