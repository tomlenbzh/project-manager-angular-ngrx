import { Component, Input, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { TaskFormName } from '../../utils/constants/task.form.constants';
import { IProject } from '../../utils/interfaces/project.interface';
import { IAddTask, ITask } from '../../utils/interfaces/task.interface';

@Component({
  selector: 'app-project-task-list',
  templateUrl: './project-task-list.component.html',
  styleUrls: ['./project-task-list.component.scss']
})
export class ProjectTaskListComponent implements OnInit {
  @Input() project!: IProject | null;

  @Input() completedTasks: ITask[] | null = [];
  @Input() onGoingTasks: ITask[] | null = [];

  @Output() created: EventEmitter<IAddTask> = new EventEmitter<IAddTask>();
  @Output() updated: EventEmitter<ITask> = new EventEmitter<ITask>();
  @Output() deleted: EventEmitter<ITask> = new EventEmitter<ITask>();

  @ViewChild('modal', { static: true }) private modal!: ModalComponent;

  form!: FormGroup;
  controlNames = TaskFormName;

  constructor(private formBuilder: FormBuilder) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  get titleCtrl() {
    return this.form.get(this.controlNames.TITLE);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.createForm();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  addOneTask(): void {
    this.createForm();
    this.modal.open();
  }

  submitForm(): void {
    if (this.form.valid) {
      const modalId = this.modal.id;
      const task: ITask = this.form.getRawValue();
      const payload: IAddTask = { modalId, task };

      this.created.emit(payload);
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private createForm(): void {
    this.form = this.formBuilder.group({
      [this.controlNames.TITLE]: new FormControl('', [Validators.required])
    });
  }
}
