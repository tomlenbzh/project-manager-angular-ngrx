import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { ProjectFormName } from '../../utils/constants/project-form.constants';
import { IAddUpdateProject, IProject } from '../../utils/interfaces/project.interface';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit, OnChanges {
  @Input() onGoingProjects!: IProject[] | null;
  @Input() completedProjects!: IProject[] | null;
  @Input() userId!: number;

  @ViewChild('modal', { static: true }) private modal!: ModalComponent;

  @Output() private created: EventEmitter<IAddUpdateProject> = new EventEmitter<IAddUpdateProject>();
  @Output() private edited: EventEmitter<IAddUpdateProject> = new EventEmitter<IAddUpdateProject>();
  @Output() private deleted: EventEmitter<IProject> = new EventEmitter<IProject>();

  form!: FormGroup;
  controlNames = ProjectFormName;

  modalType: 'NEW' | 'EDIT' = 'NEW';

  editedProject!: IProject;
  selectedProject!: IProject | null;

  constructor(private formBuilder: FormBuilder) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  get titleCtrl() {
    return this.form.get(this.controlNames.TITLE);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.createForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      ('onGoingProjects' in changes && this.onGoingProjects) ||
      ('completedProjects' in changes && this.completedProjects)
    ) {
      const allProjects = [...this.onGoingProjects!, ...this.completedProjects!];
      const selectedProject = allProjects.find((project) => project.id === this.selectedProject?.id);
      this.selectedProject = !selectedProject ? null : selectedProject;
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  completedProject(project: IProject): void {
    const payload: IAddUpdateProject = { modalId: '', project };
    this.edited.emit(payload);
  }

  selectProject(project: IProject): void {
    this.selectedProject = project;
  }

  addOneProject(): void {
    this.createForm();
    this.modalType = 'NEW';
    this.modal.open();
  }

  editNewProject(project: IProject): void {
    this.createForm();
    this.updateForm(project);
    this.modalType = 'EDIT';
    this.modal.open();
  }

  removeOnProject(project: IProject): void {
    this.deleted.emit(project);
  }

  submitForm(): void {
    if (this.form.valid) {
      const modalId = this.modal.id;
      const rawFormValue = this.form.getRawValue();
      const project: IProject = this.modalType === 'NEW' ? rawFormValue : { ...this.editedProject, ...rawFormValue };
      const payload = { modalId, project };

      this.modalType === 'NEW' ? this.created.emit(payload) : this.edited.emit(payload);
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private createForm(): void {
    this.form = this.formBuilder.group({
      [this.controlNames.TITLE]: new FormControl('', [Validators.required]),
      [this.controlNames.DESCRIPTION]: new FormControl('', []),
      [this.controlNames.STARTS_ON]: new FormControl(null, []),
      [this.controlNames.DUE_ON]: new FormControl(null, [])
    });
  }

  private updateForm(project: IProject): void {
    this.editedProject = project;
    this.form.patchValue({
      [this.controlNames.TITLE]: project?.title || null,
      [this.controlNames.DESCRIPTION]: project?.description || null,
      [this.controlNames.STARTS_ON]: project?.startsOn || null,
      [this.controlNames.DUE_ON]: project?.dueOn || null
    });
  }
}
