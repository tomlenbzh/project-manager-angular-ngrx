export interface ITask {
  id?: number;
  title?: string;
  createdAt?: Date;
  updatedAt?: Date;
  user?: { id: number };
  project?: { id: number };
  completed?: boolean;
}

export interface IAddTask {
  modalId: string;
  task: ITask;
}
