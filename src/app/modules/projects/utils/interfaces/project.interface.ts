export interface IProject {
  id?: number;
  title?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
  startsOn?: Date;
  dueOn?: Date;
  tasksTotal?: number;
  tasksDone?: number;
  user?: { id: number };
  completed?: boolean;
}

export interface IAddUpdateProject {
  modalId: string;
  project: IProject;
}
