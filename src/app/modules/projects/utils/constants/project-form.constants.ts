export enum ProjectFormName {
  TITLE = 'title',
  DESCRIPTION = 'description',
  STARTS_ON = 'startsOn',
  DUE_ON = 'dueOn'
}
