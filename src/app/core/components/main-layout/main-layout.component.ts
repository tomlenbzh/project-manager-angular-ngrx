import { Component, Input, SimpleChanges, OnChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatDrawer, MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { TranslateService } from '@ngx-translate/core';
import { IUser } from 'src/app/modules/auth/utils/user.interface';
import { ACCEPTED_LANGUAGES, LANG, SELECTED_LANGUAGE } from 'src/app/utils/constants/lang';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnChanges {
  @Input() isOnTop: boolean = true;
  @Input() isLargeViewport: boolean = true;
  @Input() user!: IUser | null;

  @Output() loggedOut: EventEmitter<any> = new EventEmitter<any>();
  @Output() langChanged: EventEmitter<IUser> = new EventEmitter<IUser>();

  @ViewChild('drawer') drawer!: MatDrawer;
  @ViewChild('sidenav') sidenav!: MatSidenav;

  fixed: boolean = true;
  opened: boolean = true;
  top: number = 0;
  bottom: number = 0;
  mode: MatDrawerMode = 'side';

  acceptedLangs = ACCEPTED_LANGUAGES;

  constructor(private translate: TranslateService) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('user' in changes && this.user) this.setCurrentLang();

    if ('isLargeViewport' in changes) {
      if (this.isLargeViewport && this.sidenav?.opened) this.toggleDrawer();
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  toggleDrawer(): void {
    this.drawer.toggle();
  }

  logout(): void {
    this.loggedOut.emit();
  }

  changeLang(lang: LANG): void {
    const user: IUser = { ...this.user, lang };
    this.langChanged.emit(user);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private setCurrentLang(): void {
    this.translate.setDefaultLang(this.user?.lang!);
    this.translate.use(this.user?.lang!);
    localStorage.removeItem(SELECTED_LANGUAGE);
    localStorage.setItem(SELECTED_LANGUAGE, this.user?.lang!);
  }
}
