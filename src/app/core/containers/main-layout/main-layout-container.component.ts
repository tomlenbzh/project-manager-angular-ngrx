import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { IUser } from 'src/app/modules/auth/utils/user.interface';
import { AuthHelper } from 'src/app/store/auth/auth.helper';
import { UserHelper } from 'src/app/store/user/user.helper';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-main-layout-container',
  template: `<app-main-layout
    [user]="user | async"
    [isLargeViewport]="isLargeViewport"
    (loggedOut)="logout()"
    (langChanged)="updateUser($event)"
  ></app-main-layout>`
})
export class MainLayoutContainerComponent implements AfterViewInit, OnDestroy, OnInit {
  user!: Observable<IUser | null>;

  isOnTop = true;
  isLargeViewport = false;

  private smallBreakPoint = '(min-width: 768px)';

  constructor(
    public breakpointObserver: BreakpointObserver,
    private authHelper: AuthHelper,
    private userHelper: UserHelper,
    private router: Router
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit() {
    this.user = this.userHelper.user();
    this.fetchUser();

    this.breakpointObserver
      .observe([this.smallBreakPoint])
      .subscribe((state: BreakpointState) => (this.isLargeViewport = state.matches));
  }

  ngAfterViewInit(): void {
    window.addEventListener('scroll', this.onScroll.bind(this), true);
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.onScroll, true);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  logout(): void {
    this.authHelper.logout();
  }

  updateUser(user: IUser): void {
    this.userHelper.updateUser(user);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private fetchUser(): void {
    const token: string | null = this.authHelper.getAccessToken();

    if (token) {
      const userId: number | undefined = this.decodeToken(token);
      userId && this.userHelper.fetchUser(userId);
    } else {
      this.router.navigateByUrl('auth');
    }
  }

  /**
   * Checks if has scrolled pas 50px in viewport.
   *
   * @param       { any }      event
   */
  private onScroll(event: any): void {
    if (event.srcElement.scrollTop > 50) {
      this.isOnTop = false;
    } else {
      this.isOnTop = true;
    }
  }

  /**
   * Returns the user id from the acees token.
   *
   * @param     { string }      token
   * @returns   { number | undefined }
   */
  private decodeToken(token: string): number | undefined {
    const decoded: any = jwt_decode(token);
    const user: IUser = decoded?.user;
    return user.id;
  }
}
